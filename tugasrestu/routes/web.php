<?php


//Route::get('/', 'HomeController@index');
//Route::get('/register', 'AuthController@form');
//Route::post('/welcome', 'AuthController@come');
    Route::get('/', function(){
        return view ('layout.items.index1');
    });  
   Route::get('/data-tables', function(){
       return view ('layout.items.table');
   });  
//
//Route::get('/master', function(){
//    return view ('layout.master');
//   });
   Route::get('/pertanyaan', 'PertanyaanController@index');
   Route::get('/pertanyaan/create', 'PertanyaanController@create');
   Route::post('/pertanyaan', 'PertanyaanController@store');
   Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
   Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
   Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
   Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');