<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('layout.create');
    }
    public function store(Request $request){
        //dd($request->all());
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success','Post Berhasil Disimpan');
    }public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        //dd($pertanyaan);
        return view('layout.index', compact('pertanyaan'));
    }
    public function show($id){
        $pertanyaan = DB::table('pertanyaan')->where('id',$id)->first();
        //dd($pertanyaan);
        return view('layout.show', compact('pertanyaan'));
    }
    public function edit($id){
        $pertanyaan = DB::table('pertanyaan')->where('id',$id)->first();

        return view('layout.edit', compact('pertanyaan'));
    }
    public function update($id, Request $request){

       // $request->validate([
       //     'judul' =>'required|unique:pertanyaan',
       //     'isi' => 'required'
       // ]);

        $query = DB::table('pertanyaan')
            ->where('id', $id)
            ->update([
                'judul' => $request['judul'],
                'isi' => $request['isi']
            ]);
            return redirect('/pertanyaan')->with('success','Berhasil Update Pertanyaan');
            }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id',$id)->delete();
        return redirect('/pertanyaan')->with('success','Pertanyaan berhasil dihapus');

    }
}