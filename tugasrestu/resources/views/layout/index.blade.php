@extends('layout.master')

@section('content')
<div class="card-header ">
 <h3 class="card-title">Table Question</h3>
 </div>
<div class="card-body">
@if (session('success'))
<div class="alert alert-success">
{{session('success')}}
</div>
@endif
    <a class="btn btn-primary mb-2" href="/pertanyaan/create">Create New Question</a>
     <table class="table table-bordered ">
       <thead>                  
         <tr>
           <th style="width: 10px">#</th>
           <th>Title</th>
           <th>Body</th>
           <th style="width: 40px;" >Actions</th>
         </tr>
       </thead>
       <tbody>
       @forelse($pertanyaan as $key => $pertanyaan)
       <tr>
       <td>{{$key +1}}</td>
       <td>{{$pertanyaan->judul}}</td>
       <td>{{$pertanyaan->isi}}</td>
       <td style="display:flex;">
       <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm my-1 mr-1">Detail</a>
       <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-warning btn-sm my-1">Edit</a>
       <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
       @csrf
       @method('DELETE')
       <input type="submit" value="Delete"class="btn btn-danger btn-sm my-1 ml-1">
       </form>
       </td>
       </tr>
       @empty
       <tr>
       <td colspan="4" align="center">No Question</td>
       </tr>
       @endforelse
      </tbody>  
    <!--   <div class="card-footer clearfix">
       <tfoot>
     <ul class="pagination pagination-sm m-0 float-right">
       <li class="page-item"><a class="page-link" href="#">«</a></li>
       <li class="page-item"><a class="page-link" href="#">1</a></li>
       <li class="page-item"><a class="page-link" href="#">2</a></li>
       <li class="page-item"><a class="page-link" href="#">3</a></li>
       <li class="page-item"><a class="page-link" href="#">»</a></li>
     </ul>
     </tfoot>
   </div> -->
     </table>
   </div>
@endsection