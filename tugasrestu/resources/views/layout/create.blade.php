@extends ('layout.master')

@section ('content')
<div class="card card-primary mt-1 ml-1 mr-1">
              <div class="card-header">
                <h3 class="card-title">Write your questions below !</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Title</label>
                    <input type="text" class="form-control"  name="judul" placeholder="Input Title">
                  </div>
                  <div class="form-group">
                    <label for="isi">Body</label>
                    <input type="text" class="form-control"  name="isi" placeholder="Input Body">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
@endsection