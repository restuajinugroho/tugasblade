@extends ('layout.master')

@section ('content')
<div class="card card-primary mt-1 ml-1 mr-1">
              <div class="card-header">
                <h3 class="card-title">Edit question {{$pertanyaan->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Title</label>
                    <input type="text" class="form-control"  name="judul" placeholder="Input Title" value="{{old('title',$pertanyaan->judul)}}">
                  </div>
                  <div class="form-group">
                    <label for="isi">Body</label>
                    <input type="text" class="form-control"  name="isi" placeholder="Input Body" value="{{old('title',$pertanyaan->isi)}}">
                </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
@endsection